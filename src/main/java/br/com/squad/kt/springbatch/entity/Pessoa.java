package br.com.squad.kt.springbatch.entity;

public class Pessoa {
	private int idade;
	private String nome;
	private String cargo;
	private String grupo;
	
	public Pessoa() {
		
	}
	
	public Pessoa(int idade, String nome, String cargo) {
		this.idade = idade;
		this.nome = nome;
		this.cargo = cargo;
	}
	
	public Pessoa(int idade, String nome, String cargo, String grupo) {
		this.idade = idade;
		this.nome = nome;
		this.cargo = cargo;
		this.grupo = grupo;
	}
	
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	
	
}
