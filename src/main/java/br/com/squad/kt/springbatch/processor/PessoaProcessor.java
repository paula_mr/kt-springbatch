package br.com.squad.kt.springbatch.processor;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import br.com.squad.kt.springbatch.entity.Pessoa;

public class PessoaProcessor implements ItemProcessor<Pessoa, Pessoa> {
	
	private final String[] GRUPO_PESSOAS = {"MENOR", "ADULTO", "IDOSO"};
	
	private Logger logger = Logger.getLogger(PessoaProcessor.class);
	
	public Pessoa process(Pessoa pessoa) throws Exception {
		logger.info("Processando pessoa: " + pessoa.getNome());
		
		if (pessoa.getIdade() < 18) {
			pessoa.setGrupo(GRUPO_PESSOAS[0]);
		}
		else if (pessoa.getIdade() > 59) {
			pessoa.setGrupo(GRUPO_PESSOAS[2]);
		}
		else {
			pessoa.setGrupo(GRUPO_PESSOAS[1]);
		}
		
		logger.info("Pessoa " + pessoa.getNome() + " processada com sucesso!");

		return pessoa;
	}

}
