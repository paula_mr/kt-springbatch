package br.com.squad.kt.springbatch.repository;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import br.com.squad.kt.springbatch.entity.Pessoa;

public class PessoaRepository {
	private DataSource dataSource;
	private JdbcTemplate manager;
	
	private Logger logger = Logger.getLogger(PessoaRepository.class);
	
	public void insert(Pessoa pessoa) {
		String sql = "INSERT INTO TBL_PESSOA(NM_PESSOA, NR_IDADE, DS_CARGO, DS_GRUPO) VALUES (?,?,?,?)";
		
		logger.info("Query " + sql);
		
		manager.update(sql, pessoa.getNome(), pessoa.getIdade(), pessoa.getCargo(), pessoa.getGrupo());
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	public JdbcTemplate getManager() {
		return manager;
	}
	public void setManager(JdbcTemplate manager) {
		this.manager = manager;
	}
	
	
}
