package br.com.squad.kt.springbatch;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
	
	private Application() {
	}
	
	private static Logger logger = Logger.getLogger(Application.class);
	
	public static void main(String[] args) {
							
		String jobName = "jobPessoa";
		
			@SuppressWarnings("resource")
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
					"/batch-application-config.xml");
						
			JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
			Job job = (Job) context.getBean(jobName);
																	
			JobParametersBuilder jobParameters = new JobParametersBuilder();

			JobParameters parameters = jobParameters.toJobParameters();
			
			try {
				JobExecution execution = jobLauncher.run(job, parameters);
				logger.info("Execucao: " + execution.getExitStatus().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			
	}
	
	
	
}