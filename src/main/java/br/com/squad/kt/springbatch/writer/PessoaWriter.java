package br.com.squad.kt.springbatch.writer;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import br.com.squad.kt.springbatch.entity.Pessoa;
import br.com.squad.kt.springbatch.repository.PessoaRepository;

public class PessoaWriter implements ItemWriter<Pessoa>{

	public PessoaRepository repository;
	
	private Logger logger = Logger.getLogger(PessoaWriter.class);

	
	public void write(List<? extends Pessoa> lista) throws Exception {
		
		logger.info("Recebidas " + lista.size() + " para salvar");
		
		for (Pessoa p: lista) {
			repository.insert(p);
		}
		
		logger.info("Pessoas salvas com sucesso!");
		
	}

	public PessoaRepository getRepository() {
		return repository;
	}

	public void setRepository(PessoaRepository repository) {
		this.repository = repository;
	}
	
	

}
