package br.com.squad.kt.springbatch.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.squad.kt.springbatch.entity.Pessoa;

public class PessoaMapper implements FieldSetMapper<Pessoa> {

	public Pessoa mapFieldSet(FieldSet fieldSet) throws BindException {
		Pessoa pessoa = new Pessoa();
		pessoa.setNome(fieldSet.readString("nome"));
		pessoa.setIdade(fieldSet.readInt("idade"));
		pessoa.setCargo(fieldSet.readString("cargo"));
		return pessoa;
	}

}
